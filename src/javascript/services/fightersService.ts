import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../Models/fighter';
import { FighterDto } from '../ModelsDTO/fighterDto';

export async function getFighters():Promise<FighterDto[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET') as Promise<FighterDto[]>;
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id:number):Promise<FighterDto> {
  try {
    const endpoint = 'details/fighter/${id}.json';
    const apiResult = await callApi(endpoint, 'GET') as Promise<FighterDto>;
    return apiResult;
  } catch (error) {
    throw error;
  }
  // endpoint - `details/fighter/${id}.json`;
}

