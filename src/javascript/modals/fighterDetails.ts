import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from '../Models/fighter';

export  function showFighterDetailsModal(fighter:Fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal(title, bodyElement);
}

function createFighterDetails(fighter:Fighter) : HTMLElement {
  const { Name,Damage, Defence, Health, CrirticalDamage, Source } = fighter;

  const fighterDetails = createElement( 'div', 'modal-body');
  const DefenceElement = createElement('span', 'fighter-name');
  const DamageElement = createElement('span', 'fighter-name');
  const HealthElement = createElement('span', 'fighter-name');
  const nameElement = createElement('span', 'fighter-name');
  const imgElement = createElement('img', 'fighter-img', {src:Source, alt:'avatar of fighter'});
  const criticalDamageElement = createElement('span', 'fighter-name');

  DefenceElement.innerText ='\nDamage: '+ String(Damage);
  DamageElement.innerText = '\nAttack: '+ String(Defence);
  HealthElement.innerText='\nHealth: '+ String(Health);
  nameElement.innerText = 'Name: '+ Name;
  criticalDamageElement.innerText='\nCritical damage: '+ String(CrirticalDamage)+'\n';

  fighterDetails.append(nameElement);
  fighterDetails.append(DamageElement);
  fighterDetails.append(DefenceElement);
  fighterDetails.append(HealthElement);
  fighterDetails.append(criticalDamageElement);
  fighterDetails.append(imgElement);

  return fighterDetails;
}
