import { createElement, getElementsByClassName } from '../helpers/domHelper';

export function showModal(title:string, bodyElement:HTMLElement) {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement); 
  
  root.append(modal);
}

function getModalContainer() : HTMLElement {
  return document.getElementById('root');
}

function createModal(title:string, bodyElement:Element):HTMLElement {
  const layer = createElement( 'div','modal-layer');
  const modalContainer = createElement('div','modal-root');
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title:string):HTMLElement {
  const headerElement = createElement('div','modal-header');
  const titleElement = createElement('span');
  const closeButton = createElement( 'div','close-btn' );
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal(event:Event):void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}

export function clearCheckBoxByClassName(className:string) {
  for(let element of getElementsByClassName(className)){
    (element as HTMLInputElement).checked=false;
  }
}