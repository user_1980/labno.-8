import { createElement } from "../helpers/domHelper";
import { Fighter } from "../Models/fighter";
import { showModal } from "./modal";
export  function showWinnerModal(fighter:Fighter) {
  const winnerElement = createElement('div', 'winner');
  const imageElement = createElement('img', 'fighter-img', {src:fighter.Source, alt:'winners ava'});
  winnerElement.append(imageElement);
  showModal('The winner is: '+fighter.Name, winnerElement);
  // show winner name and image
}