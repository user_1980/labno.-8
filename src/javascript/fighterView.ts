import { createElement } from './helpers/domHelper'; 
import { Dictionary } from './Models/dictionary';
import { Fighter } from './Models/fighter';
import { FighterDto } from './ModelsDTO/fighterDto';

export function createFighter(fighter:FighterDto, handleClick:(e:Event, fighter:FighterDto)=>Promise<void>, selectFighter:(e:Event, fighter:Fighter)=>Promise<void>) {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement('div', 'fighter');
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev:Event) => ev.stopPropagation();
   const onCheckboxClick = (ev:Event) => selectFighter(ev, {id:Number(fighter._id)});
   const onFighterClick = (ev:Event) => handleClick(ev, fighter);

   fighterContainer.addEventListener('click', onFighterClick, false);
   checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name:string) {
  const nameElement = createElement( 'span', 'name' );
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source:string) {
  const attributes = { src: source };
  const imgElement = createElement( 'img', 'fighter-image', attributes);

  return imgElement;
}

function createCheckbox() {
  const label = createElement( 'label', 'custom-checkbox');
  const span = createElement('span','checkmark' );
  const attributes:Dictionary = { type: 'checkbox' };
  const checkboxElement = createElement('input','check-box' ,attributes);

  label.append(checkboxElement, span);
  return label;
}