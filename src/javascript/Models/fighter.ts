export interface Fighter{
    id: number;
    Name?:string;
    Damage?:number;
    Health?:number;
    Defence?:number;
    CrirticalDamage?:number;
    dodgeChance?:number;
    Source?:string;
}