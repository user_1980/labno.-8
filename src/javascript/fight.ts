import { Fighter } from "./Models/fighter";

export function fight(firstFighter:Fighter, secondFighter:Fighter):Fighter {
  while(firstFighter.Health>0 && secondFighter.Health>0){
     secondFighter.Health=secondFighter.Health-getDamage(firstFighter, secondFighter);
     if(secondFighter.Health<0){
       console.log(firstFighter.Health);
       return firstFighter;
     }
     firstFighter.Health=firstFighter.Health-getDamage(secondFighter, firstFighter);
     if(firstFighter.Health<0){
      console.log(secondFighter.Health);
       return secondFighter;
     }
  }
}

export function getDamage(attacker:Fighter, enemy:Fighter):number {
  let power:number = getHitPower(attacker) - getBlockPower(enemy); 
  if(power<0.5){
    return 0.5;
  }
  return power;
}

export function getHitPower(fighter:Fighter):number {
  if(Math.random()<0.5){
    return fighter.Damage;
  }
  return fighter.Damage*fighter.CrirticalDamage;
}

export function getBlockPower(fighter:Fighter):number {
  return fighter.Defence * fighter.dodgeChance;
}
