import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Fighter } from './Models/fighter';
import { FighterDto } from './ModelsDTO/fighterDto';
import { fightersDetails } from './helpers/mockData';
import { clearCheckBoxByClassName } from './modals/modal';

export function createFighters(fighters:FighterDto[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement( 'div','fighters' );
  
 fightersContainer.append(...fighterElements);

  return fightersContainer;
}


const fightersDetailsCache = new Map<number, Fighter>();

async function showFighterDetails(event:Event, fighter:FighterDto):Promise<void> {
  const fullInfo = await getFighterInfo(Number(fighter._id));
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId:number):Promise<Fighter>  {
  let fighterDetails:Fighter;
  for(let f of fightersDetails){
    if(f._id==String(fighterId)){
      fighterDetails = {
        id:fighterId,
        Name:f.name,
        Damage:f.attack,
        Health:f.health,
        Defence:f.defense,
        CrirticalDamage:1+Math.random(),
        Source:f.source,
        dodgeChance:1+Math.random()
      };
    }
  }
  return fighterDetails;
  // get fighter form fightersDetailsCache or use getFighterDetails function
}

function createFightersSelector() {
  const selectedFighters = new Map<number, Fighter>();

  return async function selectFighterForBattle(event:Event, fighter:Fighter) {
    const fullInfo = await getFighterInfo(fighter.id);

    if (event.target) {
      selectedFighters.set(fighter.id, fullInfo);
    } else { 
      selectedFighters.delete(fighter.id);
    }

    if (selectedFighters.size === 2) {
      let entities =Array.from(selectedFighters.values());
      const winner = fight(entities[0], entities[1]);
      showWinnerModal(winner);
      selectedFighters.clear();
      clearCheckBoxByClassName('check-box');
    }
  }
}
