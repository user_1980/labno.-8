import { Dictionary } from "../Models/dictionary";

export function createElement( tagName:string, className?:string, attributes:Dictionary = {}):HTMLElement {
  const element = document.createElement(tagName);
  
  if (className) {
    element.classList.add(className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
}

export function getElementsByClassName(className:string):Element[] {
  return Array.from(document.getElementsByClassName(className));
}
